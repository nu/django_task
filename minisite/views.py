from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from .forms import NodeForm
from .models import Node


def index(request):
    """
    Retrieves the index (home) page
    """
    return render(request, 'nodes/index.html', {
        'nodes_list': Node.get_annotated_list(),
    })


def node_detail(request, node_path):
    current_node = Node.get_by_path_or_404(node_path)
    return render(request, 'nodes/detail.html', {
        'node': current_node,
        'subnodes': current_node.get_descendants_annotated_list()
    })


@login_required(login_url=reverse_lazy('minisite:login'))
def node_add_edit(request, node_path=None, parent_path=None):
    """
    Shows / Validates add/edit Node form
    @todo Add user permissions validation

    :param node_path: Node path to edit. default:None to create a new Node
    :param parent_path: Parent Node path to prefill the form. Only used if node_path=None
    """
    if node_path:
        # Editing existing node
        current_node = Node.get_by_path_or_404(node_path)
        initial = {}
        title = "Edit node <strong>{}</strong>".format(current_node.title)

    elif parent_path:
        # Adding a sub-node
        parent_node = Node.get_by_path_or_404(parent_path)
        # Pre-fill the form with correct node position, but allow to change it later
        initial = {'_ref_node_id': parent_node.pk, '_position': 'sorted-child'}
        current_node = Node()
        title = "Add sub-node for <strong>{}</strong>".format(parent_node.title)

    else:
        # Adding a new root node
        current_node = Node()
        initial = {}
        title = "Add a new node"

    form = NodeForm(request.POST or None, instance=current_node, initial=initial)

    if request.POST and form.is_valid():
        saved_node = form.save()
        # Be careful with node now because http://django-treebeard.readthedocs.io/en/latest/caveats.html#raw-queries
        return redirect('minisite:node_detail', node_path=saved_node.get_path())

    return render(request, 'nodes/edit.html', {
        'form': form,
        'title': title
    })

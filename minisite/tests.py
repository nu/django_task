import string
from django.contrib.auth.models import User
from django.template import Template, Context
from django.test import TestCase
from django.urls import reverse

from .forms import NodeForm
from .models import Node

test_data = [
    {'data': {'slug': 'test_1_slug', 'title': 'Test 1 Title', 'content': 'Test 1 Content'}, 'children': [
        {'data': {'slug': 'test_1_1_slug', 'title': 'Test 1-1 Title', 'content': 'Test 1-1 Content'}, 'children': [
            {'data': {'slug': 'test_1_1_1_slug', 'title': 'Test 1-1-1 Title', 'content': 'Test 1-1-1 Content'}},
            {'data': {'slug': 'test_1_1_2_slug', 'title': 'Test 1-1-2 Title', 'content': 'Test 1-1-2 Content'}}
        ]},
        {'data': {'slug': 'test_1_2_slug', 'title': 'Test 1-2 Title', 'content': 'Test 1-2 Content'}}
    ]},
    {'data': {'slug': 'Проверка_2_Slug', 'title': 'Проверка 2 Title', 'content': 'Проверка 2 Text'}}
]


class UrlTests(TestCase):
    """Test URL schema & URL dispatcher"""

    def setUp(self):
        Node.load_bulk(test_data)
        self.client.force_login(User.objects.get_or_create(username='test_user')[0])

    def test_invalid_url(self):
        response = self.client.get('/invalid_url')
        self.assertEqual(response.status_code, 404, 'Invalid URL must return HTTP 404')

    def test_index(self):
        url, url_name, status_code = '/', 'index', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))

    def test_login(self):
        url, url_name, status_code = '/login', 'login', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))

    def test_logout(self):
        # @todo: Does it break other tests?
        url, url_name, status_code = '/logout', 'logout', 302
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))

    def test_admin(self):
        url, view_name, status_code = '/admin/', 'admin:index', 302
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.view_name, view_name, url+' must resolve to: '+view_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))

    def test_level1_node(self):
        url, url_name, status_code = '/test_1_slug', 'node_detail', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'node_path': url[1:]}, url+' args must resolve correctly')

    def test_level3_node(self):
        url, url_name, status_code = '/test_1_slug/test_1_1_slug/test_1_1_1_slug', 'node_detail', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'node_path': url[1:]}, url+' args must resolve correctly')

    def test_wrong_level_node(self):
        url, url_name, status_code = '/test_1_1_1_slug', 'node_detail', 404
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'node_path': url[1:]}, url+' args must resolve correctly')

    def test_cyrillic_upcase_node(self):
        url, url_name, status_code = '/Проверка_2_Slug', 'node_detail', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        # @fixme deal with encodings and test it too
        # self.assertDictEqual(response.resolver_match.kwargs, {'node_path': uri_to_iri(url[1:])}, url+' args must resolve correctly')

    def test_root_add(self):
        url, url_name, status_code = '/add', 'add', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertFalse(response.resolver_match.kwargs, url+' must have no args')

    def test_level1_add(self):
        url, url_name, status_code = '/test_1_slug/add', 'node_add_subnode', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'parent_path': url[1:-4]}, url+' args must resolve correctly')

    def test_level3_add(self):
        url, url_name, status_code = '/test_1_slug/test_1_1_slug/test_1_1_1_slug/add', 'node_add_subnode', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'parent_path': url[1:-4]}, url+' args must resolve correctly')

    def test_root_edit(self):
        response = self.client.get('/edit')
        self.assertEqual(response.status_code, 404, '/edit URL must return HTTP 404')

    def test_level1_edit(self):
        url, url_name, status_code = '/test_1_slug/edit', 'node_edit', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'node_path': url[1:-5]}, url+' args must resolve correctly')

    def test_level3_edit(self):
        url, url_name, status_code = '/test_1_slug/test_1_1_slug/test_1_1_1_slug/edit', 'node_edit', 200
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'node_path': url[1:-5]}, url+' args must resolve correctly')

    def test_wrong_level_node_add(self):
        url, url_name, status_code = '/test_1_1_1_slug/add', 'node_add_subnode', 404
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'parent_path': url[1:-4]}, url+' args must resolve correctly')

    def test_wrong_level_node_edit(self):
        url, url_name, status_code = '/test_1_1_1_slug/edit', 'node_edit', 404
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.url_name, url_name, url+' must resolve to: '+url_name)
        self.assertEqual(response.status_code, status_code, url+' must return HTTP '+str(status_code))
        self.assertDictEqual(response.resolver_match.kwargs, {'node_path': url[1:-5]}, url+' args must resolve correctly')


class ViewTests(TestCase):
    """Ensure that pages render every requested feature"""

    def setUp(self):
        Node.load_bulk(test_data)
        self.client.force_login(User.objects.get_or_create(username='test_user')[0])

    def test_index_page(self):
        response = self.client.get(reverse('minisite:index'))

        # Check basic page structure
        self.assertContains(response, '<!DOCTYPE html>', html=True,
                            msg_prefix='Index page must contain "<!DOCTYPE html>"')
        self.assertContains(response, '<!-- Bootstrap CSS -->', html=True,
                            msg_prefix='Index page must include Bootstrap')
        self.assertContains(response, 'jumbotron', html=False,
                            msg_prefix='Index page must contain jumbotron')

        # Check that pages are presented
        page_titles = ['Test 1 Title', 'Test 1-1 Title', 'Test 1-1-1 Title', 'Test 1-1-2 Title', 'Test 1-2 Title',
                       'Проверка 2 Title']
        for title in page_titles:
            with self.subTest(t=title):
                self.assertContains(response, title, html=True, count=1,
                                    msg_prefix='Index page must contain every Node title')
        self.assertContains(response, 'href="/test_1_slug', html=False, count=5,
                            msg_prefix='Index page must contain 5 links to test_1_slug and its descendants')
        self.assertContains(response, 'href="/%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0_2_Slug"', html=False,
                            msg_prefix='Index page must contain correctly encoded link to Test 2')

        # Check for 'Add' link
        self.assertContains(response, 'href="{}"'.format(reverse('minisite:add')), html=False,
                            msg_prefix='Index page must contain link to Add Node')

    def test_detail_page(self):
        node_path = 'test_1_slug/test_1_1_slug'
        node_detail_url = reverse('minisite:node_detail', kwargs={'node_path': node_path})
        node_edit_url = reverse('minisite:node_edit', kwargs={'node_path': node_path})
        node_add_subnode_url = reverse('minisite:node_add_subnode', kwargs={'parent_path': node_path})
        response = self.client.get(node_detail_url)

        # Check that we have Node title and content
        self.assertContains(response, '<title>Test 1-1 Title</title>', html=True,
                            msg_prefix='Node detail page must show Node title')
        self.assertContains(response, 'Test 1-1 Content', html=True,
                            msg_prefix='Node detail page must contain Node text')

        # Check for related & unrelated Node titles
        related_page_titles = ['Test 1 Title', 'Test 1-1-1 Title', 'Test 1-1-2 Title']
        for title in related_page_titles:
            with self.subTest(t=title):
                self.assertContains(response, title, html=True, count=1,
                                    msg_prefix='Node detail page must contain names of its descendants and ancestors')
        unrelated_page_titles = ['Проверка 2 Title', 'Test 1-2 Title']
        for title in unrelated_page_titles:
            with self.subTest(t=title):
                self.assertNotContains(response, title, html=True,
                                       msg_prefix='Node detail page must not contain names of unrelated pages')

        # Check for Edit & Add subnode links
        self.assertContains(response, 'href="{}"'.format(node_edit_url), html=False,
                            msg_prefix='Node detail page must contain link to Edit')
        self.assertContains(response, 'href="{}"'.format(node_add_subnode_url), html=False,
                            msg_prefix='Node detail page must contain link to Add sub-node')

    def test_edit_page(self):
        node_path = 'test_1_slug/test_1_1_slug'
        node_edit_url = reverse('minisite:node_edit', kwargs={'node_path': node_path})
        response = self.client.get(node_edit_url)

        # Check that there are all the form features, incl. editable Node fields and positioning fields
        form_tests = {
            'form tag': '<form ',
            'submit button': 'type="submit"',
            'csrf token': 'csrfmiddlewaretoken',
            'slug field': 'name="slug"',
            'slug value': 'value="test_1_1_slug"',
            'title field': 'name="title"',
            'title value': 'value="Test 1-1 Title"',
            'content field': '<textarea name="content"',
            'content value': 'Test 1-1 Content</textarea>',
            'treebeard _position field': '<select name="_position"',
            'treebeard _position value': '<option value="sorted-child" selected>',
            'treebeard _ref_node_id field': '<select name="_ref_node_id"',
            'treebeard _ref_node_id value': 'selected>[test_1_slug]',
        }
        for name, contents in form_tests.items():
            with self.subTest(n=name):
                self.assertContains(response, contents, msg_prefix='Node edit form must contain {}'.format(name))

    def test_add_subnode_page(self):
        node_path = 'test_1_slug'
        node_add_subnode_url = reverse('minisite:node_add_subnode', kwargs={'parent_path': node_path})
        response = self.client.get(node_add_subnode_url)

        # Check that there are all the form features, incl. editable Node fields and positioning fields
        form_tests = {
            'form tag': '<form ',
            'submit button': 'type="submit"',
            'csrf token': 'csrfmiddlewaretoken',
            'slug field': 'name="slug"',
            'title field': 'name="title"',
            'content field': '<textarea name="content"',
            'treebeard _position field': '<select name="_position"',
            'treebeard _position value': '<option value="sorted-child" selected>',
            'treebeard _ref_node_id field': '<select name="_ref_node_id"',
            'treebeard _ref_node_id value': 'selected>[{}]'.format(node_path),
        }
        for name, contents in form_tests.items():
            with self.subTest(n=name):
                self.assertContains(response, contents, msg_prefix='Node edit form must contain {}'.format(name))

    def test_add_page(self):
        response = self.client.get(reverse('minisite:add'))

        # Check that there are all the form features, incl. editable Node fields and positioning fields
        form_tests = {
            'form tag': '<form ',
            'submit button': 'type="submit"',
            'csrf token': 'csrfmiddlewaretoken',
            'slug field': 'name="slug"',
            'title field': 'name="title"',
            'content field': '<textarea name="content"',
            'treebeard _position field': '<select name="_position"',
            'treebeard _position value': '<option value="sorted-child" selected>',
            'treebeard _ref_node_id field': '<select name="_ref_node_id"',
            # can't easily test treebeard _ref_node_id value because no value selected
        }
        for name, contents in form_tests.items():
            with self.subTest(n=name):
                self.assertContains(response, contents, msg_prefix='Node edit form must contain {}'.format(name))


class TemplateTagsTests(TestCase):
    """Test template tags"""

    def test_wikify_tag(self):
        with self.subTest('No markup'):
            data = '** one * * two \\ \\ \\\\ three (()) ((four))'
            out = Template(
                '{% load wikify %}'
                '{{ data|wikify|safe }}'
            ).render(Context({'data': data}))
            self.assertEqual(out, data)

        with self.subTest('Bold'):
            out = Template(
                '{% load wikify %}'
                '{{ data|wikify|safe }}'
            ).render(Context({'data': 'one**t w o**three**fo\nur**five**six'}))
            self.assertEqual(out, 'one<b>t w o</b>three<b>fo\nur</b>five**six')

        with self.subTest('Italic'):
            out = Template(
                '{% load wikify %}'
                '{{ data|wikify|safe }}'
            ).render(Context({'data': 'one\\\\t w o\\\\three\\\\fo\nur\\\\five\\\\six'}))
            self.assertEqual(out, 'one<i>t w o</i>three<i>fo\nur</i>five\\\\six')

        # This one is tricky
        # with self.subTest('Bold + Italic'):
        #     out = Template(
        #         '{% load wikify %}'
        #         '{{ data|wikify|safe }}'
        #     ).render(Context({'data': 'one**two\\\\three**four\\\\five'}))
        #     self.assertEqual(out, 'one<b>two\\\\three</b>four\\\\five')

        with self.subTest('Links'):
            out = Template(
                '{% load wikify %}'
                '{{ data|wikify|safe }}'
            ).render(Context({'data': '((a/b/c one)) ((a t w o))'}))
            self.assertEqual(out, '<a href="a/b/c">one</a> <a href="a">t w o</a>')


class FormTests(TestCase):
    """Test forms & form validation"""

    default = {'slug': 's', 'title': 't', 'content': 'c', '_position': 'sorted-child'}
    cyrillic_lower = ''.join(chr(x) for x in range(ord('а'), ord('ё') + 1))
    cyrillic = cyrillic_lower + cyrillic_lower.upper()

    def test_valid_form(self):
        with self.subTest('Simple valid form'):
            form = NodeForm(data={**self.default})
            self.assertTrue(form.is_valid(), str(form.data) + form.errors.as_text())

        with self.subTest('Longer values'):
            form = NodeForm(data={**self.default, 'slug': 's' * 99, 'title': 't' * 1000, 'content': 'c' * 10000})
            self.assertTrue(form.is_valid(), str(form.data) + form.errors.as_text())

        with self.subTest('Slug: Latin alphanumeric, _'):
            form = NodeForm(data={**self.default, 'slug': string.ascii_letters + string.digits + '_'})
            self.assertTrue(form.is_valid(), str(form.data) + form.errors.as_text())

        with self.subTest('Slug: Cyrillic'):
            form = NodeForm(data={**self.default, 'slug': self.cyrillic})
            self.assertTrue(form.is_valid(), str(form.data) + form.errors.as_text())

        with self.subTest('Title: all printable'):
            form = NodeForm(data={**self.default, 'title': string.printable + self.cyrillic})
            self.assertTrue(form.is_valid(), str(form.data) + form.errors.as_text())

        with self.subTest('Content: all printable'):
            form = NodeForm(data={**self.default, 'content': string.printable + self.cyrillic})
            self.assertTrue(form.is_valid(), str(form.data) + form.errors.as_text())

    def test_missing_fields(self):
        for x in self.default.keys():
            with self.subTest('Empty {} must fail'.format(x)):
                form = NodeForm(data={**self.default, x: ''})
                self.assertFalse(form.is_valid(), str(form.data) + form.errors.as_text())

    def test_bad_slug_characters(self):
        bad_characters = [chr(x) for x in range(0, 128) if chr(x) not in string.ascii_letters + string.digits + '_']
        for x in bad_characters:
            with self.subTest('Slug with "{}" must fail'.format(x)):
                form = NodeForm(data={**self.default, 'slug': '_{}_'.format(x)})
                self.assertFalse(form.is_valid(), str(form.data) + form.errors.as_text())

    def test_bad_url_slugs(self):
        Node.load_bulk(test_data)

        # For new root nodes
        slugs_for_new_node = {
            'login': False,
            'logout': False,
            'add': False,
            'test_1_slug': False,
            'Проверка_2_Slug': False,
            'test_1_1_slug': True,
            'test_1_1_1_slug': True,
            'test_1_2_slug': True,
        }
        for slug, result in slugs_for_new_node.items():
            with self.subTest('New root node with slug={} must return {}'.format(slug, result)):
                form = NodeForm(data={**self.default, 'slug': slug})
                self.assertEqual(form.is_valid(), result, str(form.data) + form.errors.as_text())

        # For new subnodes
        parent_node_path = 'test_1_slug'
        parent_node_pk = str(Node.objects.get(slug=parent_node_path).pk)
        slugs_for_subnode = [
            ({'slug': 'login'}, True),
            ({'slug': 'logout'}, True),
            ({'slug': 'add'}, False),
            ({'slug': 'test_1_slug'}, True),
            ({'slug': 'test_1_1_slug'}, False),
            ({'slug': 'test_1_1_1_slug'}, True),
            ({'slug': 'test_1_2_slug'}, False),
            ({'_position': 'sorted-sibling', 'slug': 'test_1_slug'}, False),
            ({'_position': 'sorted-sibling', 'slug': 'test_1_1_slug'}, True),
        ]
        for form_data, result in slugs_for_subnode:
            with self.subTest('New subnode with {} must return {}'.format(str(form_data), result)):
                form = NodeForm(data={**self.default, '_ref_node_id': parent_node_pk, **form_data})
                self.assertEqual(form.is_valid(), result, str(form.data) + form.errors.as_text())

        # Sorry, it won't work that way, needs access to server
        # # For node editing
        # node_path = 'test_1_1_slug'
        # node = Node.objects.get(slug=node_path)
        # slugs_for_subnode = [
        #     ({'slug': 'login'}, True),
        #     ({'slug': 'logout'}, True),
        #     ({'slug': 'add'}, False),
        #     ({'slug': 'test_1_slug'}, True),
        #     ({'slug': 'test_1_1_slug'}, False),
        #     ({'slug': 'test_1_1_1_slug'}, True),
        #     ({'slug': 'test_1_2_slug'}, False),
        #     ({'_position': 'sorted-sibling', 'slug': 'test_1_slug'}, False),
        #     ({'_position': 'sorted-sibling', 'slug': 'test_1_1_slug'}, True),
        # ]
        # for form_updates, result in slugs_for_subnode:
        #     with self.subTest('Editing node {} with {} must return {}'.format(node_path, str(form_updates), result)):
        #         form = NodeForm(instance=node)
        #         form.data = form.initial
        #         form.data.update(**form_updates)
        #         self.assertEqual(form.is_valid(), result, str(form.is_bound) + str(form.data) + form.errors.as_text())

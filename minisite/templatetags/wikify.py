import re

from django import template

register = template.Library()


def markup_to_tags(s: str, markup_tag: str, open_tag: str, close_tag: str, escape_func=lambda x: x):
    """
    Wikifies text, replacing, for example, **String** → <b>String</b>

    :param s: Source string
    :param markup_tag: Markup tag to search
    :param open_tag: Tag to prepend marked up part
    :param close_tag: Tag to append to marked up part
    :param escape_func: function to escape string
    :return: Wikified string
    """
    # Split only even times
    s_split = list(enumerate(s.split(markup_tag, s.count(markup_tag) // 2 * 2)))
    return ''.join(
        [open_tag + escape_func(x[1]) + close_tag if x[0] % 2 else escape_func(x[1]) for x in s_split]
    )


def convert_wiki_links(s: str):
    """
    Converts wiki-style links: ((/rel/path/ String)) → <a href="/rel/path">String</a>

    :param s: Source string
    :return: Wikified string
    """
    return re.sub(r'\(\(([\w/]+) +([^)]+)\)\)', r'<a href="\1">\2</a>', s)


@register.filter()
def wikify(text):
    """ Wikifies text:
        **String** → <b>String</b>
        \\String\\ → <i>string</i>
        ((/rel/path/ String)) → <a href="/rel/path">String</a>

        This function is *unsafe*, gets any raw HTML and outputs even more raw HTML
    """
    result = markup_to_tags(text, '**', '<b>', '</b>')
    result = markup_to_tags(result, '\\\\', '<i>', '</i>')
    result = convert_wiki_links(result)
    return result

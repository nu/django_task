from django.core.exceptions import ValidationError
from treebeard.forms import movenodeform_factory

from .models import Node

# Subclassing MoveNodeForm directly is discouraged,
# since special care is needed to handle excluded fields, and these change depending on the tree type
MP_Form = movenodeform_factory(Node)


class NodeForm(MP_Form):
    def clean(self):
        # Check if slug may be used
        slug = self.cleaned_data.get('slug', '')
        position = self.cleaned_data.get('_position', None)
        ref_node_id = self.cleaned_data.get('_ref_node_id', None)
        projected_path = self.instance.get_projected_path(ref_node_id, position, slug)
        if not self.instance.is_acceptable_path(projected_path):
            raise ValidationError("This slug can not be used: path /{} is unavailable".format(projected_path))

        return super(self.__class__, self).clean()

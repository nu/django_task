from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'minisite'
urlpatterns = [
    # Index
    path('', views.index, name='index'),

    # Login / Logout
    path('login',  auth_views.login,  name='login'),
    path('logout', auth_views.logout, name='logout'),

    # Add / Edit views
    path('add', views.node_add_edit, name='add'),
    path('<path:parent_path>/add', views.node_add_edit, name='node_add_subnode'),
    path('<path:node_path>/edit',  views.node_add_edit, name='node_edit'),

    # Sic: a catch-all url pattern. Keep it last!
    path('<path:node_path>', views.node_detail, name='node_detail'),
]

from django.core.validators import RegexValidator
from django.db import models
from django.http import Http404
from django.urls import resolve
from treebeard.mp_tree import MP_Node

validate_unicode_slug_no_dashes = RegexValidator(
    r'^\w+\Z',
    "Enter a valid 'slug' consisting of Unicode letters, numbers, or underscores.",
    'invalid'
)


class Node(MP_Node):
    slug = models.SlugField(max_length=100, allow_unicode=True, validators=[validate_unicode_slug_no_dashes])
    title = models.CharField(max_length=1024)
    content = models.TextField()
    node_order_by = ['slug']

    @classmethod
    def get_by_path(cls, path: str):
        """
        Get Node by full path.
        Can throw MultipleObjectsReturned if DB is inconsistent
        :param path: path string like 'foo/bar/baz'
        :return: Node instance or None
        """
        if not path:
            return None

        try:
            next_node = None
            for path_component in path.split('/'):
                if not next_node:
                    # Explicitly find the root node, since the syntax is different
                    next_node = cls.get_root_nodes().get(slug=path_component)
                else:
                    next_node = next_node.get_children().get(slug=path_component)
            return next_node

        except cls.DoesNotExist:
            return None

    @classmethod
    def get_by_path_or_404(cls, path: str):
        """
        Get Node by full path or raise Http404
        Can throw MultipleObjectsReturned if DB is inconsistent
        :param path: path string like 'foo/bar/baz'
        :return: Node instance
        """
        n = cls.get_by_path(path)
        if n:
            return n
        else:
            raise Http404('Node not found')

    def get_descendants_annotated_list(self):
        """
        Gets __class__.get_annotated_list() for descendants
        A shortcut to use in templates
        :return: annotated tree branch of element's descendants
        """
        return self.get_annotated_list_qs(self.get_descendants()) if not self.is_leaf() else []

    def get_path(self):
        """
        Get full path of this instance
        :return: path string like 'foo/bar/baz'
        """
        if self.pk:
            path_elements = [x.slug for x in self.get_ancestors()] + [self.slug]
            return '/'.join(path_elements)
        else:
            return ''

    @classmethod
    def get_projected_path(cls, ref_node_id, position, slug):
        """
        Get full path Node will have after NodeForm is saved with given parameters
        :return: path string like 'foo/bar/baz'
        """
        if not ref_node_id:
            return slug

        ref_node = Node.objects.get(pk=ref_node_id)
        if position == 'sorted-sibling':
            # Have to try harder to find parent
            if ref_node.is_root():
                return slug
            ref_node = ref_node.get_parent()

        return '/'.join([ref_node.get_path(), slug])

    def is_acceptable_path(self, path: str):
        if not path:
            return False
        # If it's my path, it's acceptable
        if path == self.get_path():
            return True

        # Find a view for this path
        resolved_path = resolve('/' + path)
        if resolved_path.view_name != 'minisite:node_detail':
            # If path is reserved for any view but node_detail
            return False
        else:
            # if it's a node detail url - check that node with this path does not exist
            return not self.get_by_path(path)

    def __str__(self):
        return "[{}] {}".format(self.get_path(), self.title)
